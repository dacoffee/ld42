import { Module } from "vuex-dry";
import _ from "lodash";
import zones from "@/data/zones";
import buildings from "@/data/buildings";

const builderWage = 20;
const demolishEffort = 4;
const demolishCost = 100;

export default Module.build({
    state() {
        return {
            builders: 4,
            builderWage,
            queue: [],
            zones,
            buildings,
            demolishCost,
            demolishEffort,
        };
    },
    getters: {
        buildersWages: state => state.builders * builderWage,
        busyBuilders: state =>
            state.queue.reduce((acc, item) => acc + item.effort, 0),
        queueFull: (state, getters) => getters.busyBuilders >= state.builders,
        isEnoughBuilders: (state, getters) => amount =>
            state.builders - getters.busyBuilders >= amount,
        availableZones: (state, getters, rootState, rootGetters) =>
            Object.keys(state.zones).filter(
                key =>
                    state.zones[key].cost <= rootGetters["money/balance"] &&
                    state.zones[key].effort <= state.builders
            ),
        unavailableZones: (state, getters) =>
            Object.keys(state.zones).filter(
                key => !getters.availableZones.includes(key)
            ),
        isBusy: state => (x, y) =>
            state.queue.reduce(
                (acc, item) => acc || (item.x === x && item.y === y),
                false
            ),
        zoneByType: state => type => state.zones[type],
        providers: state => need =>
            state.buildings
                .filter(building => building.provides.includes(need))
                .map(building => building.name),
        canFireBuilder: state => state.builders > 4,
    },
    actions: {
        processQueue: ({ state, dispatch, commit }) => {
            let buildersLeft = state.builders;
            state.queue.forEach(item => {
                if (buildersLeft >= item.effort) {
                    buildersLeft = buildersLeft - item.effort;
                    dispatch("processQueueItem", item);
                    commit("queue$delete", queueItem =>
                        _.isEqual(queueItem, item)
                    );
                }
            });
        },
        processQueueItem: ({ dispatch }, item) => {
            if (item.cost) {
                dispatch(
                    "money/deduct",
                    {
                        amount: item.cost,
                        reason: item.type,
                    },
                    {
                        root: true,
                    }
                );
            }

            switch (item.type) {
                case "build":
                    dispatch(
                        "map/build",
                        {
                            zone: item.zone,
                            x: item.x,
                            y: item.y,
                        },
                        { root: true }
                    );
                    break;
                case "demolish":
                    dispatch(
                        "map/demolish",
                        {
                            x: item.x,
                            y: item.y,
                        },
                        { root: true }
                    );
                    break;
                default:
                    // THIS SHOULD NOT HAPPEN
                    console.log("Sanity check: invalid queue item", item); // eslint-disable-line no-console
                    break;
            }
        },
        queueBuild: ({ commit }, { type, x, y }) => {
            let zone = zones[type];
            commit("queue$add", {
                type: "build",
                cost: zone.cost,
                effort: zone.effort,
                zone: type,
                x,
                y,
            });
        },
        queueDemolish: ({ commit }, { x, y }) => {
            commit("queue$add", {
                type: "demolish",
                cost: demolishCost,
                effort: demolishEffort,
                x,
                y,
            });
        },
        hireBuilder: ({ state, commit }) => {
            commit("builders$assign", state.builders + 1);
        },
        fireBuilder: ({ state, commit }) => {
            commit("builders$assign", state.builders - 1);
        },
    },
});
