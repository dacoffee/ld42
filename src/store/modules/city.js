import { Module } from "vuex-dry";

export default Module.build({
    state() {
        return {
            name: "Dareville",
            mayor: "Mayor L. Brown",
            started: false,
        };
    },
});
