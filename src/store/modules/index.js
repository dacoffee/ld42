import map from "./map";
import city from "./city";
import money from "./money";
import construction from "./construction";
import simulation from "./simulation";

export default {
    map,
    city,
    money,
    construction,
    simulation,
};
