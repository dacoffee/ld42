import { Module } from "vuex-dry";
import _ from "lodash";

export default Module.build({
    state() {
        return {
            zones: [],
            buildings: [],
            highlightX: null,
            highlightY: null,
            highlightRadius: null,
            width: 3,
            height: 2,
            expandCost: 800,
        };
    },
    actions: {
        build: ({ state, commit, rootGetters }, { zone, x, y }) => {
            let zoneObj = rootGetters["construction/zones$get"](zone);
            let buildings = zoneObj.contains.map(buildingType => {
                let id = state.buildings.length;
                commit("buildings$add", {
                    x,
                    y,
                    id,
                    ...rootGetters["construction/buildings$get"](buildingType),
                });
                return id;
            });

            commit("zones$add", {
                type: zone,
                buildings,
                x,
                y,
            });
        },
        demolish: ({ commit }, { x, y }) => {
            commit("zones$delete", zone => zone.x === x && zone.y === y);
            commit(
                "buildings$delete",
                building => building.x === x && building.y === y
            );
        },
        doAllFinances: ({ state, dispatch }) => {
            state.buildings.forEach((building, index) =>
                dispatch("doFinanceFor", index)
            );
        },
        doFinanceFor: ({ state, commit, dispatch, getters }, index) => {
            let building = state.buildings[index];
            if (building.income) {
                let occupants = building.occupants || 1;
                let multiplier = getters.getBuildingSatisfaction(index);
                let income = Math.round(
                    building.income * occupants * multiplier
                );
                building.balance += income;
            }

            if (building.tax) {
                dispatch(
                    "money/add",
                    {
                        amount: building.tax,
                        reason: "tax:" + index,
                    },
                    { root: true }
                );
                building.balance -= building.tax;
            }

            if (building.upkeep) {
                dispatch(
                    "money/deduct",
                    {
                        amount: building.upkeep,
                        reason: "upkeep:" + index,
                    },
                    { root: true }
                );
            }

            commit("buildings$update", {
                value: building,
                test: "id",
            });
        },
        expandX: ({ state, commit }) => commit("width$assign", state.width + 1),
        expandY: ({ state, commit }) =>
            commit("height$assign", state.height + 1),
        hireBuilder: ({ state, commit }) =>
            commit("builders$assign", state.builders + 1),
        highlight: ({ commit }, { x, y, radius }) => {
            commit("highlightX$assign", x);
            commit("highlightY$assign", y);
            commit("highlightRadius$assign", radius);
        },
        unhighlight: ({ commit }) => {
            commit("highlightX$assign", null);
            commit("highlightY$assign", null);
            commit("highlightRadius$assign", null);
        },
    },
    getters: {
        grid: state => {
            let grid = _.times(state.height, () => _.times(state.width, ""));
            _.forEach(state.zones, (zone, id) => {
                let { x, y } = zone;
                grid[y][x] = id;
            });
            return grid;
        },
        isOccupied: (state, getters) => (x, y) => {
            const grid = getters.grid;
            return grid && grid[y] && grid[y][x];
        },
        inRange: () => (x1, y1, x2, y2, radius) => {
            let a = x1 - x2;
            let b = y1 - y2;
            let c = Math.sqrt(a ** 2 + b ** 2);
            return Math.floor(c) <= radius;
        },
        needsGrid: (state, getters) => {
            let grid = _.times(state.height, () =>
                _.times(state.width, _.stubArray)
            );
            state.buildings.forEach(building => {
                let { x: x1, y: y1, radius } = building;
                grid.forEach((row, y2) => {
                    row.forEach((lot, x2) => {
                        if (getters.inRange(x1, y1, x2, y2, radius)) {
                            building.provides.forEach(need => lot.push(need));
                        }
                    });
                });
            });
            return grid;
        },
        getBuildingSatisfaction: (state, getters) => index => {
            let grid = getters.needsGrid;
            let { x, y, needs } = state.buildings[index];
            let satisfiedNeeds = grid[y][x];
            return _.intersection(satisfiedNeeds, needs).length / needs.length;
        },
        costExpandX: state => state.expandCost * state.height,
        costExpandY: state => state.expandCost * state.width,
        countExpandX: state => state.height,
        countExpandY: state => state.width,
        canExpandX: (state, getters, rootState, rootGetters) =>
            getters.costExpandX <= rootGetters["money/balance"],
        canExpandY: (state, getters, rootState, rootGetters) =>
            getters.costExpandY <= rootGetters["money/balance"],
        highlightGrid: (state, getters) => {
            let grid = _.times(state.height, () =>
                _.times(state.width, _.stubFalse)
            );
            if (state.highlightX && state.highlightY && state.highlightRadius) {
                grid.forEach((row, y) =>
                    row.forEach((lot, x) => {
                        if (
                            getters.inRange(
                                state.highlightX,
                                state.highlightY,
                                x,
                                y,
                                state.highlightRadius
                            )
                        ) {
                            grid[y][x] = true;
                        }
                    })
                );
            }
            return grid;
        },
    },
});
