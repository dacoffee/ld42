import { Module } from "vuex-dry";

const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
];

const startYear = 2004;

export default Module.build({
    state() {
        return {
            monthNo: 0,
        };
    },
    actions: {
        nextMonth: ({ state, dispatch, commit }) => {
            dispatch("construction/processQueue", null, { root: true });
            dispatch("money/nextMonth", null, { root: true });
            commit("monthNo$assign", state.monthNo + 1);
        },
    },
    getters: {
        month: (state, getters) => getters.monthFromNo(state.monthNo),
        year: (state, getters) => getters.yearFromNo(state.monthNo),
        monthFromNo: () => monthNo => monthNames[monthNo % 12],
        yearFromNo: () => monthNo => startYear + Math.floor(monthNo / 12),
    },
});
