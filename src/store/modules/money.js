import { Module } from "vuex-dry";

export default Module.build({
    state() {
        return {
            balance: 4000,
            transactions: [],
            loans: [],
            loanCounter: 0,
            availableLoans: [500, 2000, 6000, 12000],
        };
    },
    actions: {
        add: ({ dispatch }, { amount, reason }) => {
            dispatch("transaction", {
                amount,
                reason,
            });
        },
        deduct: ({ dispatch }, { amount, reason }) => {
            dispatch("transaction", {
                amount: -amount,
                reason,
            });
        },
        transaction: ({ state, commit, rootGetters }, { amount, reason }) => {
            commit("balance$assign", state.balance + amount);
            commit("transactions$add", {
                amount,
                reason,
                month: rootGetters["simulation/monthNo"],
            });
        },
        nextMonth: ({ state, commit, dispatch, rootGetters }) => {
            dispatch("deduct", {
                amount: rootGetters["construction/buildersWages"],
                reason: "wages",
            });

            state.loans.forEach(({ rate, months, id }) => {
                dispatch("deduct", {
                    amount: rate,
                    reason: "loan-pay",
                });

                commit("loans$update", {
                    value: {
                        rate,
                        months: months - 1,
                        id,
                    },
                    test: x => x.id === id,
                });
            });

            dispatch("map/doAllFinances", null, { root: true });
        },
        takeLoan: ({ state, getters, commit }, { amount, months }) => {
            let rate = getters.getLoanRate(amount, months);
            commit("loanCounter$assign", state.loanCounter + 1);
            commit("loans$add", {
                rate,
                months,
                id: state.loanCounter,
            });
        },
    },
    getters: {
        canLoan: ({ state }) => state.loans.length <= 2,
        getLoanRate: () => (amount, months) =>
            Math.ceil((amount * (1 + 0.1 * months)) / months),
    },
});
