import Vue from "vue";
import Vuex from "vuex";
import { plugin } from "vuex-dry";

import modules from "./modules";

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [plugin],
    modules,
});
