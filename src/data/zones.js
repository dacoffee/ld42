export default {
    houses: {
        contains: ["house", "house", "house", "house"],
        effort: 1,
        cost: 200,
        name: "Houses",
        description: "A nice place to live. (upgradable)",
    },
    apartments: {
        contains: [
            "apartment_block",
            "apartment_block",
            "apartment_block",
            "apartment_block",
        ],
        effort: 4,
        cost: 1600,
        name: "Apartment Blocks",
        description: "Lots of rentable accommodation in a condensed area.",
    },
    shops: {
        contains: ["shop", "shop", "shop", "shop"],
        effort: 1,
        cost: 200,
        name: "Shops",
        description: "Locally-run high street shops. (upgradable)",
    },
    mall: {
        contains: ["mall_shop", "mall_shop", "mall_shop", "mall_shop"],
        effort: 8,
        cost: 2000,
        name: "Mall",
        description: "Tightly-packed shops that everyone wants to visit!",
    },
    market: {
        contains: [
            "fishmonger_market",
            "grocer_market",
            "butcher_market",
            "bakery_market",
        ],
        effort: 1,
        cost: 100,
        name: "Market Square",
        description: "Open market filled with local produce!",
    },
    supermarket: {
        contains: [
            "fishmonger_supermarket",
            "grocer_supermarket",
            "butcher_supermarket",
            "bakery_supermarket",
        ],
        effort: 4,
        cost: 600,
        name: "Supermarket",
        description:
            "Huge store with shelves stuffed with all the food you could ever want!",
    },
    factories: {
        contains: ["factory", "factory", "factory", "factory"],
        effort: 2,
        cost: 200,
        name: "Factories",
        description:
            "Small factories - the heart of industry in your city! (upgradable)",
    },
    utility: {
        contains: [
            "water_basic",
            "electricity_basic",
            "sewage_basic",
            "internet_basic",
        ],
        effort: 2,
        cost: 500,
        name: "Basic Utilities",
        description:
            "Low-capacity water, electricity, sewage and internet provision. Covers a small range.",
    },
    water: {
        contains: [
            "water_standalone",
            "water_standalone",
            "water_standalone",
            "water_standalone",
        ],
        effort: 4,
        cost: 300,
        name: "Advanced Water",
        description: "Advanced water pump. Covers a large radius.",
    },
    electricity: {
        contains: [
            "electricity_standalone",
            "electricity_standalone",
            "electricity_standalone",
            "electricity_standalone",
        ],
        effort: 4,
        cost: 300,
        name: "Advanced Electricity",
        description: "Efficient electricity plant. Covers a large radius.",
    },
    sewage: {
        contains: [
            "sewage_standalone",
            "sewage_standalone",
            "sewage_standalone",
            "sewage_standalone",
        ],
        effort: 4,
        cost: 300,
        name: "Advanced Sewage",
        description:
            "Dedicated sewage treatment facility. Covers a large radius.",
    },
    internet: {
        contains: [
            "internet_standalone",
            "internet_standalone",
            "internet_standalone",
            "internet_standalone",
        ],
        effort: 2,
        cost: 200,
        name: "Advanced Internet",
        description: "High-speed broadband facility. Covers a large radius.",
    },
};
