import Route from "vue-routisan";

import store from "@/store";

import Home from "@/views/Home";
import Game from "@/views/Game";
import Build from "@/views/Build";
import BuildDetails from "@/views/BuildDetails";
import Demolish from "@/views/Demolish";
import Construction from "@/views/Construction";
import Money from "@/views/Money";
import Loans from "@/views/Loans";
import Transactions from "@/views/Transactions";
import BuildingInfo from "@/views/BuildingInfo";

const started = (to, from, next) => {
    if (!store.getters["city/started"]) {
        next("/");
    } else {
        next();
    }
};

Route.view("/", Home).name("home");
Route.group({ beforeEnter: started }, () => {
    Route.view("/game", Game)
        .name("game")
        .children(() => {
            Route.group(
                {
                    prefix: "/zone",
                },
                () => {
                    Route.view("/:x/:y/build", Build).name("build");
                    Route.view("/:x/:y/build/:type", BuildDetails).name(
                        "buildDetails"
                    );
                    Route.view("/:x/:y/demolish", Demolish).name("demolish");
                }
            );
            Route.view("/construction", Construction).name("construction");
            Route.view("/money", Money).name("money");
            Route.view("/money/loans", Loans).name("loans");
            Route.view("/money/history", Transactions).name("transactions");
            Route.view("/buildings/:id", BuildingInfo).name("buildingInfo");
        });
});

export default Route.all();
